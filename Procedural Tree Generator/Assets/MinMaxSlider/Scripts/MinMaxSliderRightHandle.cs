﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class MinMaxSliderRightHandle : Selectable, IDragHandler {
	[SerializeField] MinMaxSlider minMaxSlider;
	[SerializeField] RectTransform bounds;
	[SerializeField] RectTransform rectTransform;

	public RectTransform RectTransform {
		get { return rectTransform; }
	}

	public void OnDrag(PointerEventData eventData) {
		Vector2 position;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bounds, eventData.position, eventData.pressEventCamera, out position)) {
			float minDistance = bounds.rect.width * minMaxSlider.MinDifference;

			float minMovement = minMaxSlider.MinHandle.RectTransform.localPosition.x + minDistance;

			if(position.x < minMovement     ) position.x = minMovement;
			if(position.x > bounds.rect.xMax) position.x = bounds.rect.xMax;

			position.y = 0;

			minMaxSlider.CurrentMax = Mathf.Clamp01(position.x / bounds.rect.width);

			rectTransform.localPosition = position;

			minMaxSlider.UpdateFill ();

			minMaxSlider.OnValueChanged.Invoke ();
		}
	}
}
