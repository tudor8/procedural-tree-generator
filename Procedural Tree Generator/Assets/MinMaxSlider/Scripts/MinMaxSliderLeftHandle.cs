﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class MinMaxSliderLeftHandle : Selectable, IDragHandler {
	[SerializeField] MinMaxSlider minMaxSlider;
	[SerializeField] RectTransform bounds;
	[SerializeField] RectTransform rectTransform;

	public RectTransform RectTransform {
		get { return rectTransform; }
	}

	public void OnDrag(PointerEventData eventData) {
		Vector2 position;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bounds, eventData.position, eventData.pressEventCamera, out position)) {
			float minDistance = bounds.rect.width * minMaxSlider.MinDifference;

			float maxMovement = minMaxSlider.MaxHandle.RectTransform.localPosition.x - minDistance;

			if(position.x < bounds.rect.xMin) position.x = bounds.rect.xMin;
			if(position.x > maxMovement     ) position.x = maxMovement;

			position.y = 0;

			minMaxSlider.CurrentMin = Mathf.Clamp01(position.x / bounds.rect.width);

			rectTransform.localPosition = position;

			minMaxSlider.UpdateFill ();

			minMaxSlider.OnValueChanged.Invoke ();
		}
	}
}
