﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	/**
	 * Utility class to hold references to 3 indexes of vertices. 
	 * Basically a wrapper for triangle indexes to make it a bit more readable.
	 */
	public class Triangle {
		[SerializeField] int v1;
		[SerializeField] int v2;
		[SerializeField] int v3;

		public Triangle(int v1, int v2, int v3) {
			this.v1 = v1;
			this.v2 = v2;
			this.v3 = v3;
		}

		public int V1 {
			get { return v1; }
			set { v1 = value; }
		}

		public int V2 {
			get { return v2; }
			set { v2 = value; }
		}

		public int V3 {
			get { return v3; }
			set { v3 = value; }
		}

		public void Subtract(int value) {
			v1 -= value;
			v2 -= value;
			v3 -= value;

			if (v1 < 0 || v2 < 0 || v3 < 0) {
				Debug.LogError ("Value of triangle index fell below 0 after subtracting");
			}
		}

		public Triangle SubtractAndMakeNewTriangle(int value) {
			Triangle triangle = new Triangle (v1 - value, v2 - value, v3 - value);

			if (triangle.v1 < 0 || triangle.v2 < 0 || triangle.v3 < 0) {
				Debug.LogError ("Value of triangle index fell below 0 after subtracting");
			}

			return triangle;
		}

		public void SwitchDirection() {
			int v1c = v1;
			int v3c = v3;

			v1 = v3c;
			v3 = v1c;
		}

		/*
		 * Transform a given list of triangles into an array of int (which is used by meshes).
		 */
		public static int[] GetTriangleArray(List<Triangle> triangles) {
			int[] triangleArray = new int[triangles.Count * 3];
			for (int i = 0; i < triangles.Count; i++) {
				Triangle triangle = triangles [i];
				triangleArray [i * 3    ] = triangle.v1;
				triangleArray [i * 3 + 1] = triangle.v2;
				triangleArray [i * 3 + 2] = triangle.v3;
			}
			return triangleArray;
		}
	}
}