﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	[System.Serializable]
	public class MinMaxInt{
		[SerializeField] AnimationCurve randomnessCurve;
		[SerializeField] int min;
		[SerializeField] int max;

		void OnValidate() {
			Min = min;
			Max = max;
		}

		public int Min { 
			get { return min; } 
			set { min = value; if (min > max) {min = max;}}
		}

		public int Max { 
			get { return max; } 
			set { max = value; if (max < min) {max = min;}}
		}

		private int GetRandomValue(int min, int max, System.Random random = null) {
			if(random == null) 
				return Random.Range (min, max);
			return random.Next (min, max);
		}

		private float GetRandomValue(System.Random random = null) {
			if(random == null) 
				return Random.Range (0f, 1f);
			return (float)random.NextDouble ();
		}

		public int GetRandom(System.Random random = null) {
			int number = GetRandomValue (min, max + 1, random);

			return number;
		}

		public int GetRandomWithCurve(System.Random random = null) {
			float number = GetRandomValue (random);
			int result = (int)(randomnessCurve.Evaluate (number) * (max - min)) + min;

			return result;
		}
	}
}