﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	public class TreeScript : MonoBehaviour {
		// Imports
		public TextureGenerator textureGenerator;
		[SerializeField] public List<MeshFilter> meshFilters;
		[SerializeField] List<MeshRenderer> meshRenderers;
		[SerializeField] int verticeForChange = 64000;
		[SerializeField] MeshRenderer meshRenderer;
		[SerializeField] GameObject testPrefab ;
		[SerializeField] GameObject filterParent;
		[SerializeField] GameObject recycleParent;
		[SerializeField] Material materialToUse;

		// Game Settings
		[SerializeField] int verticeCount;
		[SerializeField] int sides; // How many sides the branches will have
		[SerializeField] int seed;
		[SerializeField] float width ;
		[SerializeField] float height;
		[SerializeField] int minimumHeight; // How many sides the branches will have
		[SerializeField] RandomChanceValues childrenChance;
		[SerializeField] MinMaxInt angleChance;
		[SerializeField] MinMaxFloat scaleChance;
		[SerializeField] Gradient colorGradient;

		[SerializeField] System.Random randomData;
		[SerializeField] List<MeshData> meshDatas; // The generated mesh data, split so that the vertice limit is under the cap


		[SerializeField] public float stageToSetTo;
		[SerializeField] int totalStages;
		[SerializeField] int estimatedVerticesPerNewBranch;
		[SerializeField] float growth;
		public int branchCount;

		public int Sides {
			get { return sides; }
			set { sides = value; }
		}

		public float Width {
			get { return width; }
			set { width = value; }
		}

		public float Height {
			get { return height; }
			set { height = value; }
		}

		public int MinimumHeight {
			get { return minimumHeight; }
			set { minimumHeight = value; }
		}

		public RandomChanceValues ChildrenChance {
			get { return childrenChance; }
		}

		public MinMaxInt AngleChance {
			get { return angleChance; }
		}

		public MinMaxFloat ScaleChance {
			get { return scaleChance; }
		}

		public System.Random RandomData {
			get { return randomData; }
		}

		public List<MeshData> MeshData {
			get { return meshDatas; }
		}

		public Gradient ColorGradient {
			get { return colorGradient; }
		}

		public MeshData CurrentMeshData {
			get { return meshDatas [meshDatas.Count - 1]; }
		}

		public int EstimatedVerticesPerNewBranch {
			get { return estimatedVerticesPerNewBranch; }
		}

		Branch rootBranch;

		void OnValidate() {
			childrenChance.OnValidate ();
		}

		public void Randomize() {
			seed = Random.Range (0, 10000);

			Generate ();
		}

		public void SpawnNewFilter() {
			GameObject filterObj = Instantiate (testPrefab) as GameObject;
			filterObj.transform.SetParent (filterParent.transform, false);

			MeshFilter filter = filterObj.GetComponent<MeshFilter> ();
			MeshRenderer renderer = filterObj.GetComponent<MeshRenderer> ();
			meshFilters.Add(filter);
			meshRenderers.Add (renderer);

			Material material = new Material (materialToUse);
			material.mainTexture = textureGenerator.GenerateTexture (textureGenerator.GenerateColorPallete (colorGradient));
			renderer.sharedMaterial = material;
		}

		public bool CheckForNewFilter(List<MeshData> meshDataToCheck, int newVerticeCount) {
			if ((meshDataToCheck [meshDataToCheck.Count - 1].verticeList.Count + estimatedVerticesPerNewBranch) > verticeForChange) {
				meshDataToCheck.Add (new MeshData ());
				if (meshDataToCheck.Count > meshFilters.Count) {
					SpawnNewFilter ();
				}
				return true;
			}
			return false;
		}

		public void Generate(float percentage) {
			Debug.Log ("hm");
			growth = percentage;
			Generate ();
		}

		public void GenerateNewTexture() {
			Material material = new Material (materialToUse);
			material.mainTexture = textureGenerator.GenerateTexture (textureGenerator.GenerateColorPallete (colorGradient));
			for (int i = 0; i < meshRenderers.Count; i++) {
				meshRenderers [i].sharedMaterial = material;
			}
		}
			
		public void Generate() {
			estimatedVerticesPerNewBranch = sides * 2 * 3; // Sides * flat shading * number of segments
				
			randomData = new System.Random (seed);

			meshDatas = new List<MeshData> ();
			meshDatas.Add (new MeshData ());

			if (meshFilters.Count == 0) {
				SpawnNewFilter ();
			}

			rootBranch = Branch.CreateNewBranch (this, new Vector3 (), Vector3.up, Vector3.right, 1f);

			List<Branch> currentSet = new List<Branch> ();
			currentSet.Add (rootBranch);

			totalStages = 0;
			while (currentSet.Count != 0) {
				List<Branch> newSet = new List<Branch> ();
				foreach (Branch branch in currentSet) {
					newSet.AddRange (branch.GenerateChildren (randomData, 1f));
				}
				currentSet = newSet;

				totalStages++;
			}
			// Delete the filters that we don't need
			while(filterParent.transform.childCount > meshDatas.Count) {
				filterParent.transform.GetChild(0).SetParent (recycleParent.transform);
				meshFilters.RemoveAt (0);
			}

			for (int i = 0; i < meshDatas.Count; i++) {
				meshFilters [i].sharedMesh = meshDatas [i].ToMesh ();
			}

			SetToPercentage (growth);
		}

		public void SetToPercentage(float percentage) {
			SetToStage (percentage * totalStages);
		}

		public void SetToStage(float stage) {
			float growthPercentage = stage / totalStages;
			int stagesCreated = 0;
			int currentMeshDataIndex = 0;
			List<Branch> currentSet = new List<Branch> (); // Holds the current branches. Children of branches get added as they are processed.
			List<Branch> lastSetOfBranches = new List<Branch> (); // Holds the latest processed branches. Resets when the currentMeshDataIndex changes.
			List<BranchSegment> segmentsToTriangulize = new List<BranchSegment> (); // Holds all segments that must be triangulized.
			List<MeshData> newMeshDatas = new List<MeshData> (); // Holds the new mesh datas
			currentSet.Add (rootBranch);
			newMeshDatas.Add (new MeshData ());
			while (currentSet.Count != 0 && stagesCreated < (int)stage + 1) {
				List<Branch> newSet = new List<Branch> ();
				for(int i = 0; i < currentSet.Count; i++) {
					Branch branch = currentSet [i];
					// Once this happens, we know that we can include a whole mesh filter without checking what to include
					if (branch.FilterIndex > currentMeshDataIndex) {
						newMeshDatas [currentMeshDataIndex] = meshDatas [currentMeshDataIndex];
						newMeshDatas.Add (new MeshData ());
						currentMeshDataIndex++;
						lastSetOfBranches.Clear ();
					}
						
					lastSetOfBranches.Add (branch);
					newSet.AddRange (branch.children);

					for (int j = 0; j < branch.segments.Count; j++) {
						BranchSegment segment = branch.segments [j];
						segment.SetWidth ((width / 2) * growthPercentage * segment.Scale.x);

						if (segment.Changed) {
							segment.ResetHeight(branch.segments [j - 1]);
						}
					}

					// Found a segment that needs to be triangulized
					if (stagesCreated == ((int)stage)) {
						BranchSegment previousSegment = branch.segments [branch.segments.Count - 2];
						BranchSegment currentSegment = branch.segments [branch.segments.Count - 1];

						float distance = Vector3.Distance (currentSegment.Center, previousSegment.Center);

						currentSegment.AddHeight (-distance * (1 - stage % 1));
						currentSegment.Changed = true;
							
						segmentsToTriangulize.Add (currentSegment);
					} 
					else if (branch.children.Count == 0) {
						BranchSegment currentSegment = branch.segments [branch.segments.Count - 1];
						segmentsToTriangulize.Add (currentSegment);
					}
				}
				currentSet = newSet;
				stagesCreated++;
			}
				
			// Add the last set of branches. We are assured at this point that there will be less than <verticeCount> points
			for (int branchIndex = 0; branchIndex < lastSetOfBranches.Count; branchIndex++) {
				Branch branch = lastSetOfBranches [branchIndex];
				for (int segmentIndex = 0; segmentIndex < branch.segments.Count; segmentIndex++) {
					BranchSegment segment = branch.segments [segmentIndex];

					for (int pointIndex = 0; pointIndex < segment.Points.Length; pointIndex++) {
						newMeshDatas [currentMeshDataIndex].verticeList.Add(meshDatas[currentMeshDataIndex].verticeList[segment.Points[pointIndex]]);
						newMeshDatas [currentMeshDataIndex].uvsList    .Add(meshDatas[currentMeshDataIndex].uvsList    [segment.Points[pointIndex]]);
					}
					// Triangles are between 2 segments and as such will be 1 less set than segments
					if (segmentIndex != branch.segments.Count - 1) {
						newMeshDatas [currentMeshDataIndex].triangleList.AddRange (branch.triangles [segmentIndex]);
					}
				}	
			}

			// Triangulate the end of all branches
			int verticesPerNewSegment = sides * 2;
			for (int i = 0; i < segmentsToTriangulize.Count; i++) {
				if (CheckForNewFilter (newMeshDatas, verticesPerNewSegment)) {
					currentMeshDataIndex++;
				}
				
				BranchSegment segment = segmentsToTriangulize [i];

				int[] points = new int[segment.Points.Length];
				List<Vector3> pointLocations = new List<Vector3> ();
				for (int j = 0; j < segment.Points.Length; j++) {
					Vector3 location = segment.MeshData.verticeList [segment.Points [j]];
					newMeshDatas [newMeshDatas.Count - 1].verticeList.Add (location);
					newMeshDatas [newMeshDatas.Count - 1].uvsList.Add (segment.MeshData.uvsList [segment.Points [j]]);
					pointLocations.Add (location);
					points [j] = newMeshDatas [newMeshDatas.Count - 1].verticeList.Count - 1;
				}

				BranchSegment endSegment = new BranchSegment (this);
				endSegment.Center = segment.Center;
				endSegment.Direction = segment.Direction;
				endSegment.Points = points;
				endSegment.MeshData = newMeshDatas [newMeshDatas.Count - 1];

				BranchSegment.TriangulateSegment (endSegment, 0.1f);
			}

				
			// Delete the filters that we don't need
			for (int i = 0; i < meshFilters.Count; i++) {
				meshFilters [i].mesh.Clear ();
			}

			for (int i = 0; i < newMeshDatas.Count; i++) {
				meshFilters [i].sharedMesh = newMeshDatas [i].ToMesh ();
			}
		}
	}
} 