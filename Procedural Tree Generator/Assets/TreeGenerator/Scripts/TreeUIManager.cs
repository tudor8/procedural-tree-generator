﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TreeGenerator {
	/**
	 * Provide methods that UI elements shall call when their value changes.
	 */
	public class TreeUIManager : MonoBehaviour {
		public delegate void Action();
		[SerializeField] TreeScript treeScript;
		[SerializeField] Rotate rotateScript;
		[SerializeField] CameraScript cameraScript;
		[SerializeField] ColorControllerUI leafColor;
		[SerializeField] ColorControllerUI branchColor;
		[SerializeField] MinMaxSlider angleSlider;
		[SerializeField] MinMaxSlider percentageSlider;
		[SerializeField] List<Slider> childrenChanceSliders;
		[SerializeField] Slider widthSlider;
		[SerializeField] Slider heightSlider;
		[SerializeField] Slider sidesSlider;
		[SerializeField] Slider speedSlider;
		[SerializeField] Slider cameraSlider;
		[SerializeField] Slider minimumHeightSlider;
		[SerializeField] Toggle autoUpdateToggle;
		[SerializeField] bool locked;
		[SerializeField] bool autoUpdate = true;

		void Start() {
			treeScript.Width = UsefulMethods.GetValueBetweenMinMax (widthSlider.value, 0.5f, 4);
			treeScript.Height = UsefulMethods.GetValueBetweenMinMax  (heightSlider.value, 1, 6);
			treeScript.Sides = (int)sidesSlider.value;
			rotateScript.Speed = UsefulMethods.GetValueBetweenMinMax  (speedSlider.value, 0, 80);
			cameraScript.Percentage = cameraSlider.value;
			treeScript.MinimumHeight = (int)UsefulMethods.GetValueBetweenMinMax  (minimumHeightSlider.value, 1, 4);
			treeScript.AngleChance.Min = (int)UsefulMethods.GetValueBetweenMinMax  (angleSlider.CurrentMin, 5, 45);
			treeScript.AngleChance.Max = (int)UsefulMethods.GetValueBetweenMinMax  (angleSlider.CurrentMax, 5, 45);
			treeScript.ScaleChance.Min = UsefulMethods.GetValueBetweenMinMax  (percentageSlider.CurrentMin, 0.35f, 0.90f);
			treeScript.ScaleChance.Max = UsefulMethods.GetValueBetweenMinMax  (percentageSlider.CurrentMax, 0.35f, 0.90f);

			treeScript.Generate ();
			SetLeafColor ();
			SetBranchColor ();
			treeScript.Generate ();

			autoUpdate = autoUpdateToggle.isOn;
		}

		public void Lock() {
			locked = true;
			//StartCoroutine (LockRoutine (() => {
			//	locked = false;
			//}));
		}

		public void SetAutoUpdate() {
			if (!autoUpdate) {
				treeScript.Generate ();
			}
			autoUpdate = !autoUpdate;
		}


		public IEnumerator LockRoutine(Action action) {
			for (int i = 0; i < 3; i++) {
				yield return new WaitForEndOfFrame ();
			}
			action ();
		}

		public void SetGrowthPercentage(float percentage) {
			if (locked) return;
			else Lock();

			treeScript.SetToPercentage (percentage);

			locked = false;
		}

		public void SetWidth(float percentage) {
			if (locked) return;
			else Lock();

			treeScript.Width = UsefulMethods.GetValueBetweenMinMax (percentage, 0.5f, 4);
			if (autoUpdate) {
				treeScript.Generate ();
			}

			locked = false;
		}

		public void SetHeight(float percentage) {
			if (locked) return;
			else Lock();

			treeScript.Height = UsefulMethods.GetValueBetweenMinMax  (percentage, 1, 6);
			if (autoUpdate) {
				treeScript.Generate ();
			}

			locked = false;
		}

		public void SetSides(float percentage) {
			if (locked) return;
			else Lock();

			treeScript.Sides = (int)percentage;
			if (autoUpdate) {
				treeScript.Generate ();
			}

			locked = false;
		}

		public void SetRotationSpeed(float percentage) {
			if (locked || !autoUpdate) return;
			else Lock();

			rotateScript.Speed = UsefulMethods.GetValueBetweenMinMax  (percentage, 0, 80);

			locked = false;
		}

		public void SetZoomAmount(float percentage) {
			if (locked || !autoUpdate) return;
			else Lock();

			cameraScript.Percentage = percentage;

			locked = false;
		}

		public void SetMinimumHeight(float percentage) {
			if (locked) return;
			else Lock();

			treeScript.MinimumHeight = (int)UsefulMethods.GetValueBetweenMinMax  (percentage, 1, 4);
			if (autoUpdate) {
				treeScript.Generate ();
			}

			locked = false;
		}

		public void SetBranchAngle() {
			if (locked) return;
			else Lock();

			treeScript.AngleChance.Min = (int)UsefulMethods.GetValueBetweenMinMax  (angleSlider.CurrentMin, 5, 45);
			treeScript.AngleChance.Max = (int)UsefulMethods.GetValueBetweenMinMax  (angleSlider.CurrentMax, 5, 45);
			if (autoUpdate) {
				treeScript.Generate ();
			}

			locked = false;
		}

		public void SetBranchPercentage() {
			if (locked) return;
			else Lock();

			treeScript.ScaleChance.Min = UsefulMethods.GetValueBetweenMinMax  (percentageSlider.CurrentMin, 0.35f, 0.90f);
			treeScript.ScaleChance.Max = UsefulMethods.GetValueBetweenMinMax  (percentageSlider.CurrentMax, 0.35f, 0.90f);
			if (autoUpdate) {
				treeScript.Generate ();
			}

			locked = false;
		}

		public void SetChildrenChance() {
			if (locked) return;
			else Lock();

			List<RandomChanceValues.RandomPoint> randomPoints = new List<RandomChanceValues.RandomPoint> ();
			for (int i = 0; i < childrenChanceSliders.Count; i++) {
				int percentage =(int)UsefulMethods.GetValueBetweenMinMax  (childrenChanceSliders[i].value, 0, 100);
				randomPoints.Add(new RandomChanceValues.RandomPoint(i + 1, percentage));
			}
			treeScript.ChildrenChance.points = randomPoints;
			treeScript.ChildrenChance.OnValidate ();
			if (autoUpdate) {
				treeScript.Generate ();
			}

			locked = false;
		}

		public void SetLeafColor() {
			if (locked || !autoUpdate) return;
			else Lock();

			GradientColorKey[] gCKeys = treeScript.ColorGradient.colorKeys;
			GradientAlphaKey[] gAKeys = treeScript.ColorGradient.alphaKeys;
			gCKeys [2].color = leafColor.ColorController.SelectedColor;
			treeScript.ColorGradient.SetKeys (gCKeys, gAKeys);
			treeScript.GenerateNewTexture ();

			locked = false;
		}

		public void SetBranchColor() {
			if (locked || !autoUpdate) return;
			else Lock();

			GradientColorKey[] gCKeys = treeScript.ColorGradient.colorKeys;
			GradientAlphaKey[] gAKeys = treeScript.ColorGradient.alphaKeys;
			gCKeys [0].color = branchColor.ColorController.SelectedColor;
			gCKeys [1].color = branchColor.ColorController.SelectedColor;
			treeScript.ColorGradient.SetKeys (gCKeys, gAKeys);
			treeScript.GenerateNewTexture ();

			locked = false;
		}

		public void Randomize() {
			if (locked || !autoUpdate) return;
			else Lock();

			treeScript.Randomize ();

			locked = false;
		}
	}
}