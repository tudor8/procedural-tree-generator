﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace TreeGenerator.PropertyDrawers {
	[CustomPropertyDrawer(typeof(TreeGenerator.RandomChanceValues.RandomPoint))]
	public class RandomPointDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			EditorGUI.BeginProperty(position, label, property);

			// Draw label
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

			// Don't make child fields be indented
			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			// Calculate rects
			var valueRect      = new Rect(position.x     , position.y, 30, position.height);
			var percentageRect = new Rect(position.x + 35, position.y, 50, position.height);
			var chanceRect     = new Rect(position.x + 90, position.y, position.width - 90, position.height);

			SerializedProperty valueProp      = property.FindPropertyRelative ("value"     );
			SerializedProperty percentageProp = property.FindPropertyRelative ("percentage");
			SerializedProperty chanceProp     = property.FindPropertyRelative ("chance"    );

			// Draw fields - passs GUIContent.none to each so they are drawn without labels
			EditorGUI.PropertyField(valueRect      , valueProp     , GUIContent.none);
			EditorGUI.PropertyField(percentageRect , percentageProp, GUIContent.none);
			EditorGUI.PropertyField(chanceRect     , chanceProp    , GUIContent.none);

			// Set indent back to what it was
			EditorGUI.indentLevel = indent;

			EditorGUI.EndProperty();
		}
	}
}
#endif