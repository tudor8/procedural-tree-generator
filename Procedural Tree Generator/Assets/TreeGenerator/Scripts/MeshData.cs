﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	[System.Serializable]
	public class MeshData {
		[HideInInspector] public Vector3[] vertices ;
		[HideInInspector] public int    [] triangles;
		[HideInInspector] public Vector2[] uvs      ;
		[HideInInspector] public Color  [] colors  ;

		[HideInInspector] public List<Vector3> verticeList;
		[HideInInspector] public List<Triangle> triangleList;
		[HideInInspector] public List<Vector2> uvsList;
		[HideInInspector] public List<Color> colorList;

		public static MeshData FromMesh(Mesh mesh) {
			MeshData meshData = new MeshData ();
			meshData.vertices = mesh.vertices;
			meshData.triangles = mesh.triangles;
			meshData.uvs = mesh.uv;
			meshData.colors = mesh.colors;
			return meshData;
		}

		public static Mesh ToMesh(Vector3[] vertices, int[] triangles, Vector2[] uvs, Color[] colors) {
			Mesh mesh = new Mesh ();
			mesh.vertices  = vertices ;
			mesh.triangles = triangles;
			mesh.uv        = uvs      ;
			mesh.colors    = colors   ;
			mesh.RecalculateNormals ();
			return mesh;
		}


		public MeshData() {
			verticeList  = new List<Vector3 > ();
			triangleList = new List<Triangle> ();
			uvsList      = new List<Vector2 > ();
			colorList    = new List<Color   > ();
		}

		public MeshData(int vertexCount, int triangleCount) {
			vertices  = new Vector3[vertexCount  ];
			uvs       = new Vector2[vertexCount  ];
			colors    = new Color  [vertexCount  ];
			triangles = new int    [triangleCount];
		}

		public MeshData(int vertexCount) {
			vertices  = new Vector3[vertexCount];
			uvs       = new Vector2[vertexCount];
			colors    = new Color  [vertexCount];
			triangleList = new List<Triangle> ();
		}

		public Mesh ToMesh() {
			Mesh mesh = new Mesh ();

			if (triangleList != null && triangleList.Count != 0) {
				triangles = Triangle.GetTriangleArray (triangleList);
			}

			if (verticeList != null && verticeList.Count != 0) {
				mesh.vertices = verticeList.ToArray ();
				mesh.uv       = uvsList    .ToArray ();
				mesh.colors   = colorList  .ToArray ();
				mesh.triangles = Triangle.GetTriangleArray (triangleList);
			} 
			else {
				mesh.vertices = vertices;
				mesh.triangles = triangles;
				mesh.uv = uvs;
				mesh.colors = colors;
			}

			mesh.RecalculateNormals ();
			return mesh;
		}
	}
}