﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildCleanup : MonoBehaviour {
	void Update() {
		if (transform.childCount != 0) {
			foreach (Transform child in transform) {
				Destroy (child.gameObject);
			}
		}
	}
}
