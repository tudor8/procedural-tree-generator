﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	/**
	 * Similar to real life, trees are made of branches.
	 * Unlike real life however, my branches are made of segments (see BranchSegment) which helps abstract the building process
	 */
	public class Branch {
		[SerializeField] Vector3 directionTop  ; // The direction the branch is growing towards
		[SerializeField] Vector3 directionFront; // Perpendicular to the main direction
		[SerializeField] Vector2 scale; // Current scale compared to the original width and height
		[SerializeField] int level; // The depth of this branch, or in other words, how many parents there are to this branch
		[SerializeField] bool end; // A flag that shows whether a branch finished growing or not
		[SerializeField] int filterIndex;

		public TreeScript           treeScript; // The tree script holds settings required to calculate branch growth
		public List<BranchSegment>  segments  ; // Segments are used to hold vertice references
		public List<Branch>         children  ; // Any branch that was created with this branch as its parent
		public List<List<Triangle>> triangles ; // A double list containing the triangles that connect each segment. 
											    // Example: triangles[0] would be the list of triangles that connects segment 0 and 1

		public int FilterIndex {
			get { return filterIndex; } 
		}

		public Vector3 DirectionFront {
			get { return directionFront; }
		}

		public Branch(TreeScript treeScript, Vector3 directionTop, Vector3 directionFront) {
			this.directionTop   = directionTop;
			this.directionFront = directionFront;
			this.treeScript     = treeScript;

			segments  = new List<BranchSegment> ();
			children  = new List<Branch       > ();
			triangles = new List<List<Triangle>>();
		}

		/**
		 * Rotate the given directionFront yAngles around the directionTop axis.
		 */
		public static KeyValuePair<Vector3, Vector3> RotateDirections(Vector3 directionTop, Vector3 directionFront, float yAngles, float xAngles) {
			Vector3 newDirectionTop   = directionTop  ;
			Vector3 newDirectionFront = directionFront;

			newDirectionFront = UsefulMethods.RotateAroundAxis (directionFront, directionTop, yAngles);

			Vector3 perpendicular = UsefulMethods.RotateAroundAxis (newDirectionFront, directionTop, 90);

			newDirectionTop   = UsefulMethods.RotateAroundAxis (newDirectionTop  , perpendicular, xAngles);
			newDirectionFront = UsefulMethods.RotateAroundAxis (newDirectionFront, perpendicular, xAngles);

			return new KeyValuePair<Vector3, Vector3> (newDirectionTop, newDirectionFront);
		}

		public static Branch CreateNewBranch(
			TreeScript treeScript, 
			Vector3 center, 
			Vector3 directionTop, 
			Vector3 directionFront,
			float growthPercentage
		) {
			Vector2 bottomScale = Vector2.one;
			Vector2 topScale    = Vector2.one * treeScript.ScaleChance.GetRandomWithCurve (treeScript.RandomData);
			Branch branch = CreateNewBranch (treeScript, center, directionTop, directionFront, directionTop, directionFront, bottomScale, topScale, 1, growthPercentage, 0);
			return branch;
		}

		public static Branch CreateNewBranch(
			TreeScript treeScript, 
			Vector3 center, 
			Vector3 directionTop, 
			Vector3 directionFront, 
			Vector3 newDirectionTop, 
			Vector3 newDirectionFront, 
			Vector2 scaleOld,
			Vector2 scaleNew,
			int level,
			float growthPercentage,
			int filterIndex
		) {
			Vector3 newCenter = center + newDirectionTop * treeScript.Height * scaleOld.y;

			Branch newBranch = new Branch (treeScript, newDirectionTop, newDirectionFront);
			newBranch.filterIndex = filterIndex;
			newBranch.level = level;

			BranchSegment firstSegment  = new BranchSegment (treeScript, treeScript.CurrentMeshData, center   , directionTop   , directionFront   , scaleOld, 1 - scaleOld.x);
			BranchSegment secondSegment = new BranchSegment (treeScript, treeScript.CurrentMeshData, newCenter, newDirectionTop, newDirectionFront, scaleNew, 1 - scaleNew.x);

			List<Triangle> newTriangles = BranchSegment.TriangulateSegments (firstSegment, secondSegment);

			newBranch.triangles.Add (newTriangles);

			newBranch.segments.Add (firstSegment);
			newBranch.segments.Add (secondSegment);

			treeScript.CurrentMeshData.triangleList.AddRange (newTriangles);

			return newBranch;
		}

		public List<Branch> GenerateChildren(System.Random random, float growthPercentage) {
			if (treeScript.branchCount > 1000000) {
				Debug.Log ("wtf?");
				return new List<Branch> ();
			}
			if (children.Count == 0 && !end) {
				if (treeScript.CheckForNewFilter (treeScript.MeshData, treeScript.EstimatedVerticesPerNewBranch)) {
					
				}

				int childrenCount = treeScript.ChildrenChance.GetRandom (random);
				int angle = (int) (random.NextDouble () * 360); // Starting angle between 0 and 359
				int anglesBetweenBranches = (int) (360 / childrenCount);
				int anglesBetweenSides    = (int) (360 / treeScript.Sides);

				if (level < treeScript.MinimumHeight) { childrenCount = 1; } 

				List<float> scales = new List<float> ();
				for (int i = 0; i < childrenCount; i++) {
					scales.Add(treeScript.ScaleChance.GetRandomWithCurve (random));
				}
					
				for (int i = 0; i < childrenCount; i++) {
					// The purpose of those calculations is to make wider branches closer to the previous branches direction
					float percentage = UsefulMethods.GetPercentageBetweenMinMax (scales[i], treeScript.ScaleChance.Min, treeScript.ScaleChance.Max);
					float angles     = UsefulMethods.GetValueBetweenMinMax (1 - percentage, treeScript.AngleChance.Min, treeScript.AngleChance.Max);

					int yAngles = UsefulMethods.RoundToClosestMultiplier (angle, anglesBetweenSides);

					if (childrenCount == 1) { angles = 0; }

					KeyValuePair<Vector3, Vector3> newDirections = RotateDirections (directionTop, directionFront, yAngles, angles);
					Vector3 rotatedFront = UsefulMethods.RotateAroundAxis (directionFront, directionTop, yAngles);
					Vector2 newScale = scales [i] * segments [segments.Count - 1].Scale;
						
					Branch child = CreateNewBranch (treeScript, segments [segments.Count - 1].Center, directionTop, rotatedFront, newDirections.Key, newDirections.Value, segments [segments.Count - 1].Scale, newScale, level + 1, growthPercentage, treeScript.MeshData.Count - 1);

					treeScript.branchCount++;
					// End of the branch, generate a new segment to close it off
					if (newScale.x < 0.1) { 
						child.end = true; 
					} 

					children.Add (child);

					angle += anglesBetweenBranches;
				}
				return children;
			}
			return new List<Branch>();
		}
	}
}