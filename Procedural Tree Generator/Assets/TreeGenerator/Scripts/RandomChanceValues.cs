﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	[System.Serializable]
	public class RandomChanceValues {
		[System.Serializable]
		public class RandomPoint {
			public int value;
			public int percentage;
			public float chance;

			public RandomPoint(int value, int percentage) {
				this.value = value;
				this.percentage = percentage;
			}

			public override string ToString () {
				return string.Format ("[RandomPoint [{0}][{1}][{2}]]", value, percentage, chance);
			}
		}

		public List<RandomPoint> points;

		public void OnValidate() {
			float totalValues = 0;
			points.ForEach (delegate(RandomPoint point) {
				totalValues += point.percentage;
			});

			if (totalValues == 0)
				return;
			
			points.ForEach (delegate(RandomPoint point) {
				point.chance = point.percentage / totalValues * 100;
			});
		}

		private int GetRandomValue(int min, int max, System.Random random = null) {
			if(random == null) 
				return Random.Range (min, max);
			return random.Next (min, max);
		}

		public int GetRandom(System.Random random = null) {
			List<RandomPoint> pointsCopy = new List<RandomPoint> (points);
			pointsCopy.Sort ((a, b) => (
				a.chance.CompareTo(b.chance)
			));

			int number = GetRandomValue (0, 100, random);
			float valueSoFar = 0;
			for (int i = 0; i < pointsCopy.Count; i++) {
				valueSoFar += pointsCopy [i].chance;
				if (number < valueSoFar) {
					return pointsCopy [i].value;
				}
			}
			Debug.LogError ("Failed to get random value");
			return 0;
		}
	}
}