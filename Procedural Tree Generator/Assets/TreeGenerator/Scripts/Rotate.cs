﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	/**
	 * Rotate the chosen object <speed> angles per second around the y axis.
	 */
	public class Rotate : MonoBehaviour {
		[SerializeField] GameObject obj;
		[SerializeField] float speed;

		public float Speed {
			get { return speed; }
			set { speed = value; }
		}

		void Update() {
			obj.transform.Rotate (new Vector3 (0, speed * Time.deltaTime, 0));
		}
	}
}