﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	/**
	 * Useful methods that aren't implemented by unity (I think?)
	 */
	public class UsefulMethods : MonoBehaviour {
		/**
		 * Rotate a point around another point by a specified angle vector.
		 */
		public static Vector3 RotateAroundPoint(Vector3 point, Vector3 origin, Vector3 angles) { 
			return Quaternion.Euler (angles) * (point - origin) + origin;
		}

		/**
		 * Rotate a point around an axis by a specific amount of angles
		 */
		public static Vector3 RotateAroundAxis(Vector3 point, Vector3 axis, float angles) { 
			return Quaternion.AngleAxis(angles, axis) * point;
		}

		/**
		 * Round an integer to the closest multiplier.
		 * Example: number = 200, multiplier = 150 -> result = 150 (300 is the other closest)
		 */
		public static int RoundToClosestMultiplier(int number, int multiplier) {
			float x = (float)number / multiplier;
			return (int)(Mathf.Round (x) * multiplier);
		}

		/**
		 * Given the minimum and maximum bounds and a percentage, find what value lies at that percentage
		 * Example: percentage = 0.5f, min = 10, max = 100 -> result = 55
		 */
		public static float GetValueBetweenMinMax(float percentage, float min, float max) {
			return (max - min) * percentage + min; 
		}

		/**
		 * Given the minimum and maximum bounds and a value, find what percentage the value is between the bounds
		 * Example: value = 55, min = 10, max = 100 -> result = 0.5f
		 */
		public static float GetPercentageBetweenMinMax(float value, float min, float max) {
			return (value - min) / (max - min);
		}
	}
}