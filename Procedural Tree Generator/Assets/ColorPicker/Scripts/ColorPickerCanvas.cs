﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/**
 * Additional class for the ColorPicker class to allow click and drag events to happen over the whole object rather than just the select object.
 * 
 * Author - Tudor Gheorghe
 */

public class ColorPickerCanvas : MonoBehaviour, IPointerDownHandler, IDragHandler {
	[SerializeField] ColorPicker colorPicker;

	public void OnPointerDown(PointerEventData eventData) {
		colorPicker.OnDrag(eventData);
	}

	public void OnDrag(PointerEventData eventData) {
		colorPicker.OnDrag(eventData);
	}
}
