﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/**
 * Dragable Object that is bound to the given rect transform.
 * Dragging the object around will update the controller based on its x and y coordinates (which translate to saturation and value)
 * The Rect Transform should have the ColorPickerCanvas class attached to handle on click and on drag events on the object itself.
 * 
 * Author - Tudor Gheorghe
 */

public class ColorPicker : MonoBehaviour, IDragHandler {
	[SerializeField] RectTransform bounds;
	[SerializeField] Image currentColor;
	[SerializeField] ColorController colorController;
	[SerializeField] ColorControllerUI colorControllerUI;

	public RectTransform Transform2D {
		get { return bounds; }
	}

	public Image CurrentColor {
		get { return currentColor; }
	}

	public void OnDrag(PointerEventData eventData) {
		Vector2 position;

		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bounds, eventData.position, eventData.pressEventCamera, out position)) {
			if(position.x < bounds.rect.xMin) position.x = bounds.rect.xMin;
			if(position.x > bounds.rect.xMax) position.x = bounds.rect.xMax;
			if(position.y < bounds.rect.yMin) position.y = bounds.rect.yMin;
			if(position.y > bounds.rect.yMax) position.y = bounds.rect.yMax;

			transform.localPosition = position;
			// Not adding a small amount seems to cause a weird bug when moving the selector to the left or bottom edge
			Color color = Color.HSVToRGB(
				colorController.Hue, 
				position.x / bounds.rect.width  + 0.001f,
				position.y / bounds.rect.height + 0.001f
			);
			color.a = colorController.Alpha;

			colorControllerUI.SetColor (color);

		}
	}
}
