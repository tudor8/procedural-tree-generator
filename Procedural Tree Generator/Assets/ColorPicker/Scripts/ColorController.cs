﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Main class to calculate the current color and handle what happens when the values are changed.
 * Also makes sure to update the texture.
 * HSV changes will automatically update the RGB values and vice versa.
 * 
 * Author - Tudor Gheorghe
 */
public class ColorController : MonoBehaviour {
	[SerializeField] int   size = 128; // Size that the generated texture and sprite will have
	[SerializeField] float hue       ;
	[SerializeField] float saturation;
	[SerializeField] float value     ;
	[SerializeField] float red  ;
	[SerializeField] float green;
	[SerializeField] float blue ;
	[SerializeField] float alpha; // Or Transparency
	[SerializeField] Color color; // The current color

	Texture2D texture;

	/*********************************************************************************************************************************************************
	 * Getters and Setters for the fields
	 *********************************************************************************************************************************************************/
	public float Hue {
		set {
			hue = value;
			CalculateRGB ();
		}
		get { return hue; }
	}

	public float Saturation {
		set {
			saturation = value;
			CalculateRGB ();
		}
		get { return saturation; }
	}

	public float Value {
		set {
			this.value = value;
			CalculateRGB ();
		}
		get { return value; }
	}

	public float Red {
		set {
			red = value;
			CalculateHSV ();
		}
		get { return red; }
	}

	public float Green {
		set {
			green = value;
			CalculateHSV ();
		}
		get { return green; }
	}

	public float Blue {
		set {
			blue = value;
			CalculateHSV ();
		}
		get { return blue; }
	}

	public float Alpha {
		set {
			alpha = value;
			color.a = value;
		}
		get { return alpha; }
	}

	public int Size {
		get { return size; }
	}

	public Texture2D MainTexture {
		get { return texture; }
	}

	public Color SelectedColor {
		get { return color; }
	}

	/*********************************************************************************************************************************************************
	 * Other methods
	 *********************************************************************************************************************************************************/
	/**
	 * Generate the starting texture (and as such should be called once by another class or in an awake)
	 */
	public void GenerateTexture() {
		texture = new Texture2D (size, size);
		texture.filterMode = FilterMode.Point;

		SetToColor (color);

		UpdateTexture ();
	}

	/**
	 * Shortcut to set instantly to a specific color
	 */
	public void SetToColor(Color color) {
		red   = color.r;
		green = color.g;
		blue  = color.b;
		alpha = color.a;
		CalculateHSV ();
	}

	public void CalculateHSV() {
		color = new Color (red, green, blue);
		Color.RGBToHSV(color, out hue, out saturation, out value);
		UpdateTexture ();
	}

	public void CalculateRGB() {
		color = Color.HSVToRGB (hue, saturation, value);
		red   = color.r;
		green = color.g;
		blue  = color.b;
		UpdateTexture ();
	}

	/**
	 * The main texture is updated based on the hue value.
	 */
	public void UpdateTexture() {
		Color[] colors = new Color[size * size];

		for (var y = 0; y < size; y++) {
			for (var x = 0; x < size; x++) {
				colors[x + y * size] = Color.HSVToRGB(hue, (float) x / size, (float) y / size);
			}
		}

		texture.SetPixels(colors);
		texture.Apply();
	}
	/********************************************************************************************************************************************************/
}
