# Procedural Tree Generator

Repository for my Procedural Tree Generator.

An example can be found at http://www.tudorgheorghe.net

# Getting Started

* This is procedural generator that can generate trees based on many parameters.

* The settings can be found on TreeScript or on the ingame UI Script. All the the UI script does is change the TreeScript parameters.

* Comes along with a custom UI color picker.

# Running

In the executable folder you can find a runnable version of the application.
 
# Built With

* [Unity](https://unity.com/) - Main Engine

# Authors

* **Tudor Gheorghe** - (http://www.tudorgheorghe.net)